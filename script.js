/* Ответ на теоретический вопрос:

1. Функции нужны для того чтобы выполнять одни и те же определенные действия (алгоритм описанный в теле функции) нужное количество раз. Можно описать определенный алгоритм в функции и вызывать его (запускать) когда нужно, а не описывать его снова и снова.

2. Иногда (зачастую) для того чтобы функция выполнила определенные действия нужно задать ей значения, к которым она применит тот алгоритм, который в ней описан.*/


const firstOperand = getOperand("first");
const operator = getOperator();
const secondOperand = getOperand("second");

function getOperand(count) {
    let stringNumber;
    let operand;
    do {
        stringNumber = prompt(`Enter ${count} operand(number)`)
        operand = Number(stringNumber);
    } while (Number.isNaN(operand) || stringNumber == "");

    return operand;
}

function getOperator() {
    let operator;
    do {
        operator = prompt(`Enter operator, please (one of these signs: "+", "-", "/", "*")`);
    } while (operator != "+" && operator != "-" && operator != "*" && operator != "/");

    return operator;
}

function calc(firstNum, secondNum, operator) {

    switch (operator) {
        case '+':
            return firstNum + secondNum;
        case '-':
            return firstNum - secondNum;
        case '/':
            return firstNum / secondNum;
        case '*':
            return firstNum * secondNum;
    }
}

console.log(calc(firstOperand, secondOperand, operator));